# xournalpp-shortcuts

Install Plugin with `sudo make clean install`.

By default, the Plugin will be installed to `/usr/share/xournalpp/plugins/`.
If you want to override the Plugin directory, install with:

```
PLUGIN_DIR="my-plugin-dir" make clean install
```
